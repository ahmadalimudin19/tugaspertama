import React, { Component } from 'react'
import { Text, View } from 'react-native'

export default class App extends Component {
  render() {
    return (
      <View style={{ justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{marginTop: 20, fontSize: 30, fontWeight: 'bold', color: 'Black'}}> HALO PAK ROFI </Text>
      </View>
    )
  }
}
